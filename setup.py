from setuptools import find_namespace_packages
from setuptools import setup

setup(
    name="perplexity_api",
    version="0.2.3",
    description="An unofficial Perplexity.ai Python API",
    long_description=open("README.md", "rt", encoding="utf-8").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/robowaifudev/perplexity-api",
    project_urls={
        "Bug Report": "https://gitlab.com/robowaifudev/perplexity-api/issues"
    },
    author="robowaifudev",
    author_email="robowaifudev@protonmail.com",
    license="MIT",
    packages=["perplexity_api"],
    install_requires=[
        "markdownify", "bs4", "selenium"
    ],
    classifiers=[
        "License :: OSI Approved :: MIT",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "Natural Language :: English",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
)
